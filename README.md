# blockhain-lab1

## Instructions

Build the docker compose

```
docker-compose build
```

Run the docker compose file
```
docker-compose up -d
```

You should see the following output:
```
Starting blockhain-lab1_node_two_1 ... done
Starting blockhain-lab1_node_one_1 ... done
```

Print the docker containers to get the container IDs, keep note of the ids
```
docker ps
```

Result
```
CONTAINER ID        IMAGE                      ...
b1e8c79fd390        blockhain-lab1_node_one    ...
5d93bd10ddaa        blockhain-lab1_node_two    ...
```

Now we have to run Geth on the first node and get the enode in order to connect to second node which runs Parity.

```
docker exec -it b1e8c79fd390 /bin/bash 
```

Now we need to run Geth 
```
geth --nousb --datadir data --rpc --rpcaddr 0.0.0.0 --rpcport 8545 --networkid 500 console
```

You should see it up and running and logged in to the console
The result should look something similar:
```
Welcome to the Geth JavaScript console!

instance: Geth/v1.9.10-stable-58cf5686/linux-amd64/go1.13.6
at block: 0 (Thu, 01 Jan 1970 00:00:00 UTC)
 datadir: /data
 modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0

> 
```

Now that we are logged in get the enode
```
> admin.nodeInfo.enode
```

Result:
```
"enode://d4f84011b6af8ec081206cafcba76da98004bbe31daaf7594fe7417086defea94bdf3fad930359105d2a648c2b6836d8d08529e56f99d04cdaf3680e0394b625@127.0.0.1:30303"
```

You will have different enode so keep it noted
Notice that your enode ends with ip address 127.0.0.1
When providing enode adress to the second node we must include the ip of the first container so take the ip of the first container provided by the command we run on a new console
```
 docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' b1e8c79fd390
```

The result is:
```
172.20.0.3
```

Yours might be different so keep note of it.
Now with the ip our first enode adress is
```
"enode://d4f84011b6af8ec081206cafcba76da98004bbe31daaf7594fe7417086defea94bdf3fad930359105d2a648c2b6836d8d08529e56f99d04cdaf3680e0394b625@172.20.0.3:30303"
```

On a different console enter the second container and run Parity with our first enode adress as boot node
```
docker exec -it 5d93bd10ddaa /bin/bash
```

After entering into the container run the following line to start Parity. Provide the respective values

```
parity --chain parity.json --network-id 500 --bootnodes 
"enode://d4f84011b6af8ec081206cafcba76da98004bbe31daaf7594fe7417086defea94bdf3fad930359105d2a648c2b6836d8d08529e56f99d04cdaf3680e0394b625@172.20.0.3:30303"
```

You should see something similar
```
2020-01-31 08:22:02 UTC Starting Parity-Ethereum/v2.6.8-beta-9bf6ed8-20191231/x86_64-linux-gnu/rustc1.40.0
2020-01-31 08:22:02 UTC Keys path /home/parity/.local/share/io.parity.ethereum/keys/GethTranslation
2020-01-31 08:22:02 UTC DB path /home/parity/.local/share/io.parity.ethereum/chains/GethTranslation/db/94e74eec8675100c
2020-01-31 08:22:02 UTC State DB configuration: fast
2020-01-31 08:22:02 UTC Operating mode: active
2020-01-31 08:22:03 UTC Configured for GethTranslation using Ethash engine
2020-01-31 08:22:08 UTC Public node URL: enode://27931133250d3bb0905a4a727467520c091acd0095b7e9ee2052bba662cae335b0eff578f3f13e9edc42d878710ac53eae0e3ca208ab3e0d98cf1f4b59647301@172.20.0.2:30303
```

Notice that after few seconds you will get the result
```
2020-01-31 08:22:33 UTC    1/25 peers   792 bytes chain 824 bytes db 0 bytes queue 1 KiB sync  RPC:  0 conn,    0 req/s,    0 µs
```
As you can see from the result above we are connected to 1 peer.

You can even check that our first node also is connected to the second node by running the code below in the first console that run Geth
```
admin.peers
```

Result
```
[{
    caps: ["eth/62", "eth/63", "par/1", "par/2", "par/3", "pip/1"],
    enode: "enode://27931133250d3bb0905a4a727467520c091acd0095b7e9ee2052bba662cae335b0eff578f3f13e9edc42d878710ac53eae0e3ca208ab3e0d98cf1f4b59647301@172.20.0.2:58726",
    id: "bd728f566c1f522018a4e147342876ec13a606b245bc6dc3a7b7c34ded5e05b8",
    name: "Parity-Ethereum/v2.6.8-beta-9bf6ed8-20191231/x86_64-linux-gnu/rustc1.40.0",
    network: {
      inbound: true,
      localAddress: "172.20.0.3:30303",
      remoteAddress: "172.20.0.2:58726",
      static: false,
      trusted: false
    },
    protocols: {
      eth: {
        difficulty: 200000000,
        head: "0x461baf096b4510c1a29e548fed16ed6bc748cd1794e74eec8675100c9ea817fe",
        version: 63
      }
    }
}]

```

Success.

From your nifty wallet you can connect to the rpc channel and check your account balance

Connect to the following location in your nifty wallet
```
http://172.20.0.3:8545
```
And you should see the balance

Now we need to make a transaction

On the first console that is running your Geth node
run the following to make a transaction

First unlock your account
```
> personal.unlockAccount("bf3d6f830ce263cae987193982192cd990442b53")
```

Then create a transaction
```
> var tx = {from: "bf3d6f830ce263cae987193982192cd990442b53", to: "16B4FD518eebF1FEc0bDFf829ec9cd350daBe005", value: web3.toWei(10.23, "ether")}
undefined
> personal.sendTransaction(tx, "passphrase")
```

Check your nifty wallet to see the transaction
